/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : localhost:3307
 Source Schema         : kms5

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 12/01/2022 13:58:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kms_authorization
-- ----------------------------
DROP TABLE IF EXISTS `kms_authorization`;
CREATE TABLE `kms_authorization`  (
  `TARGET_NAME` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `TIME_LIMIT_TYPE` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATOR` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RESOURCE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TARGET_ID` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `END_DATE` datetime NULL DEFAULT NULL,
  `CREATOR_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OPERATIONS` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SCOPE` int(11) NULL DEFAULT NULL,
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `START_DATE` datetime NULL DEFAULT NULL,
  `CREATE_DATE` datetime NULL DEFAULT NULL,
  `RESOURCE_TYPE` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `index_RESOURCE_ID`(`RESOURCE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_authorization
-- ----------------------------

-- ----------------------------
-- Table structure for kms_category
-- ----------------------------
DROP TABLE IF EXISTS `kms_category`;
CREATE TABLE `kms_category`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PARENTID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_category
-- ----------------------------

-- ----------------------------
-- Table structure for kms_disk
-- ----------------------------
DROP TABLE IF EXISTS `kms_disk`;
CREATE TABLE `kms_disk`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TYPE` int(11) NULL DEFAULT NULL,
  `NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAIN_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OWNER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ORDER_NO` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `index_ID`(`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_disk
-- ----------------------------
INSERT INTO `kms_disk` VALUES ('e21554a2-8cb3-4196-8e7e-2ab40204d455', 1, '后端组', '3CffNlgt9B9StIjIUPB', '__2Lc6K6lsEggXShMgQsf', 1);
INSERT INTO `kms_disk` VALUES ('__oP0irhWXGA2oZRusW1d', 8, '系统管理员', NULL, '__oP0irhWXGA2oZRusW1d', 999);

-- ----------------------------
-- Table structure for kms_edit_file
-- ----------------------------
DROP TABLE IF EXISTS `kms_edit_file`;
CREATE TABLE `kms_edit_file`  (
  `EDITOR_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EDIT_TIME` datetime NULL DEFAULT NULL,
  `FILE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_edit_file
-- ----------------------------

-- ----------------------------
-- Table structure for kms_file
-- ----------------------------
DROP TABLE IF EXISTS `kms_file`;
CREATE TABLE `kms_file`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DISK_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FOLDER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PATH` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATOR` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATOR_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TYPE` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATE_DATE` datetime NULL DEFAULT NULL,
  `LAST_MODIFY_DATE` datetime NULL DEFAULT NULL,
  `ORIGIN_TYPE` int(11) NULL DEFAULT NULL,
  `ORIGIN_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `URL` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DIMENSION` int(11) NULL DEFAULT NULL,
  `COLLECTS` int(11) NULL DEFAULT NULL,
  `SHARES` int(11) NULL DEFAULT NULL,
  `VIEWS` int(11) NULL DEFAULT NULL,
  `DOWNLOADS` int(11) NULL DEFAULT NULL,
  `LAST_DOWNLOAD_DATE` datetime NULL DEFAULT NULL,
  `LAST_COLLECT_DATE` datetime NULL DEFAULT NULL,
  `LAST_SHARE_DATE` datetime NULL DEFAULT NULL,
  `LAST_VIEW_DATE` datetime NULL DEFAULT NULL,
  `LAST_EDIT_DATE` datetime NULL DEFAULT NULL,
  `GOOD` int(11) NULL DEFAULT NULL,
  `BAD` int(11) NULL DEFAULT NULL,
  `CATEGORYS` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CATEGORYS_JSON` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `index_ID`(`ID`) USING BTREE,
  INDEX `index_DISK_ID`(`DISK_ID`) USING BTREE,
  INDEX `index_FOLDER_ID`(`FOLDER_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_file
-- ----------------------------

-- ----------------------------
-- Table structure for kms_file_category_set
-- ----------------------------
DROP TABLE IF EXISTS `kms_file_category_set`;
CREATE TABLE `kms_file_category_set`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FILE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CATEGORY_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_file_category_set
-- ----------------------------

-- ----------------------------
-- Table structure for kms_file_history
-- ----------------------------
DROP TABLE IF EXISTS `kms_file_history`;
CREATE TABLE `kms_file_history`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `USER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TITLE` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SOURCE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PATH` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MD5` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TMP` bit(1) NULL DEFAULT NULL,
  `CREATE_TIME` datetime NULL DEFAULT NULL,
  `VERSION_NO` int(11) NULL DEFAULT NULL,
  `FILE_SIZE` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_file_history
-- ----------------------------

-- ----------------------------
-- Table structure for kms_file_outsideshare_set
-- ----------------------------
DROP TABLE IF EXISTS `kms_file_outsideshare_set`;
CREATE TABLE `kms_file_outsideshare_set`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FILE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SHARE_TIMEOUT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_file_outsideshare_set
-- ----------------------------

-- ----------------------------
-- Table structure for kms_folder
-- ----------------------------
DROP TABLE IF EXISTS `kms_folder`;
CREATE TABLE `kms_folder`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DISK_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FOLDER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PATH` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATOR` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATOR_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TYPE` int(11) NULL DEFAULT NULL,
  `CREATE_DATE` datetime NULL DEFAULT NULL,
  `LAST_MODIFY_DATE` datetime NULL DEFAULT NULL,
  `ORDER_NO` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `index_ID`(`ID`) USING BTREE,
  INDEX `index_DISK_ID`(`DISK_ID`) USING BTREE,
  INDEX `index_FOLDER_ID`(`FOLDER_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_folder
-- ----------------------------
INSERT INTO `kms_folder` VALUES ('460ef5ae-6fa9-4d53-b6b4-e68349b35bb4', '收藏目录', '__oP0irhWXGA2oZRusW1d', '__oP0irhWXGA2oZRusW1d', '__oP0irhWXGA2oZRusW1d', NULL, NULL, 1, '2021-12-29 14:35:38', '2021-12-29 14:35:38', 5);
INSERT INTO `kms_folder` VALUES ('e21554a2-8cb3-4196-8e7e-2ab40204d455', '后端组', 'e21554a2-8cb3-4196-8e7e-2ab40204d455', NULL, NULL, NULL, NULL, 2, '2021-12-29 15:44:57', '2021-12-29 15:44:57', 1);
INSERT INTO `kms_folder` VALUES ('__oP0irhWXGA2oZRusW1d', '系统管理员', '__oP0irhWXGA2oZRusW1d', NULL, NULL, NULL, NULL, 2, '2021-12-29 14:35:38', '2021-12-29 14:35:38', 1);

-- ----------------------------
-- Table structure for kms_iflytek_task
-- ----------------------------
DROP TABLE IF EXISTS `kms_iflytek_task`;
CREATE TABLE `kms_iflytek_task`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FILE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATE_DATE` datetime NULL DEFAULT NULL,
  `TEXT` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `FILE_LENGTH` int(11) NULL DEFAULT NULL,
  `USER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_iflytek_task
-- ----------------------------

-- ----------------------------
-- Table structure for kms_keyword
-- ----------------------------
DROP TABLE IF EXISTS `kms_keyword`;
CREATE TABLE `kms_keyword`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CONTENT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATOR_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATOR` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATE_DATE` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_keyword
-- ----------------------------

-- ----------------------------
-- Table structure for kms_logs
-- ----------------------------
DROP TABLE IF EXISTS `kms_logs`;
CREATE TABLE `kms_logs`  (
  `DEPT_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RESOURCE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATOR` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATOR_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DEPT_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RESOURCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OPERATION_TYPE` int(11) NULL DEFAULT NULL,
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RESOURCE_TYPE` int(11) NULL DEFAULT NULL,
  `CREATE_DATE` datetime NULL DEFAULT NULL,
  `REGION` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_logs
-- ----------------------------

-- ----------------------------
-- Table structure for kms_map
-- ----------------------------
DROP TABLE IF EXISTS `kms_map`;
CREATE TABLE `kms_map`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TITLE` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SERIAL_NUMBER` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CONTENT` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `CREATOR` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATOR_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATE_DATE` datetime NULL DEFAULT NULL,
  `LAST_MODIFY_DATE` datetime NULL DEFAULT NULL,
  `REALM_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATUS` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOW_HISTORYS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `APPROVERS` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPROVER_IDS` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPROVER_HISTORYS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LAST_OPERATION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_map
-- ----------------------------

-- ----------------------------
-- Table structure for kms_member
-- ----------------------------
DROP TABLE IF EXISTS `kms_member`;
CREATE TABLE `kms_member`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TEAM_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TYPE` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_member
-- ----------------------------

-- ----------------------------
-- Table structure for kms_operation
-- ----------------------------
DROP TABLE IF EXISTS `kms_operation`;
CREATE TABLE `kms_operation`  (
  `CODE` int(11) NULL DEFAULT NULL,
  `DESCRIPTION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_operation
-- ----------------------------
INSERT INTO `kms_operation` VALUES (2, '资源预览', '2');
INSERT INTO `kms_operation` VALUES (1, '资源下载', '1');
INSERT INTO `kms_operation` VALUES (3, '资源编辑', '3');
INSERT INTO `kms_operation` VALUES (4, '资源分享', '4');
INSERT INTO `kms_operation` VALUES (8, '资源收藏', '8');

-- ----------------------------
-- Table structure for kms_permission_app_form
-- ----------------------------
DROP TABLE IF EXISTS `kms_permission_app_form`;
CREATE TABLE `kms_permission_app_form`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `USER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RESOURCE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RESOURCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RESOURCE_TYPE` int(11) NULL DEFAULT NULL,
  `FILE_OBJECT_TYPE` int(11) NULL DEFAULT NULL,
  `RESOURCE_OWNER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOWNLOAD` bit(1) NULL DEFAULT NULL,
  `PREVIEW` bit(1) NULL DEFAULT NULL,
  `EDIT` bit(1) NULL DEFAULT NULL,
  `APPROVERS` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPROVER_IDS` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REASON` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `REJECT_REASON` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `CREATE_DATE` datetime NULL DEFAULT NULL,
  `STATUS` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_permission_app_form
-- ----------------------------

-- ----------------------------
-- Table structure for kms_preview_file
-- ----------------------------
DROP TABLE IF EXISTS `kms_preview_file`;
CREATE TABLE `kms_preview_file`  (
  `PREVIEW_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PREVIEW_TIME` datetime NULL DEFAULT NULL,
  `FILE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_preview_file
-- ----------------------------

-- ----------------------------
-- Table structure for kms_privilege
-- ----------------------------
DROP TABLE IF EXISTS `kms_privilege`;
CREATE TABLE `kms_privilege`  (
  `END_DATE` datetime NULL DEFAULT NULL,
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `AUTH_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `START_DATE` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_privilege
-- ----------------------------

-- ----------------------------
-- Table structure for kms_privilege_dept_set
-- ----------------------------
DROP TABLE IF EXISTS `kms_privilege_dept_set`;
CREATE TABLE `kms_privilege_dept_set`  (
  `PRIVILEGE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DEPT_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_privilege_dept_set
-- ----------------------------

-- ----------------------------
-- Table structure for kms_privilege_operation_set
-- ----------------------------
DROP TABLE IF EXISTS `kms_privilege_operation_set`;
CREATE TABLE `kms_privilege_operation_set`  (
  `PRIVILEGE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OPERATION_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_privilege_operation_set
-- ----------------------------

-- ----------------------------
-- Table structure for kms_privilege_resource_set
-- ----------------------------
DROP TABLE IF EXISTS `kms_privilege_resource_set`;
CREATE TABLE `kms_privilege_resource_set`  (
  `RESOURCE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PRIVILEGE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RESOURCE_TYPE` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_privilege_resource_set
-- ----------------------------

-- ----------------------------
-- Table structure for kms_privilege_user_set
-- ----------------------------
DROP TABLE IF EXISTS `kms_privilege_user_set`;
CREATE TABLE `kms_privilege_user_set`  (
  `PRIVILEGE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_privilege_user_set
-- ----------------------------

-- ----------------------------
-- Table structure for kms_realm
-- ----------------------------
DROP TABLE IF EXISTS `kms_realm`;
CREATE TABLE `kms_realm`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ORDER_NO` int(11) NULL DEFAULT NULL,
  `DOMAIN_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_realm
-- ----------------------------
INSERT INTO `kms_realm` VALUES ('3efbc8c2-ab52-492b-b21f-633bfff7ad6b', '委员会1', 1, '3CffNlgt9B9StIjIUPB');

-- ----------------------------
-- Table structure for kms_role
-- ----------------------------
DROP TABLE IF EXISTS `kms_role`;
CREATE TABLE `kms_role`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `GRADE` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_role
-- ----------------------------
INSERT INTO `kms_role` VALUES ('0001', '员工', 0);
INSERT INTO `kms_role` VALUES ('0010', '部门知识管理员', 10);
INSERT INTO `kms_role` VALUES ('0020', '专委会知识管理员', 20);
INSERT INTO `kms_role` VALUES ('0100', '企业知识管理员', 100);

-- ----------------------------
-- Table structure for kms_share_record
-- ----------------------------
DROP TABLE IF EXISTS `kms_share_record`;
CREATE TABLE `kms_share_record`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FILE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SHARE_AUTHOR_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SHARE_RECEIVE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SHARE_RECEIVE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_share_record
-- ----------------------------

-- ----------------------------
-- Table structure for kms_stage
-- ----------------------------
DROP TABLE IF EXISTS `kms_stage`;
CREATE TABLE `kms_stage`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TEAM_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FOLDER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ORDER_NO` int(11) NULL DEFAULT NULL,
  `COMMENTS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `CREATE_DATE` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_stage
-- ----------------------------

-- ----------------------------
-- Table structure for kms_subscription
-- ----------------------------
DROP TABLE IF EXISTS `kms_subscription`;
CREATE TABLE `kms_subscription`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `USER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CONTENT_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CONTENT_TYPE` int(11) NULL DEFAULT NULL,
  `CONTENT_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATE_DATE` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_subscription
-- ----------------------------

-- ----------------------------
-- Table structure for kms_subscription_notice
-- ----------------------------
DROP TABLE IF EXISTS `kms_subscription_notice`;
CREATE TABLE `kms_subscription_notice`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `USER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RESOURCE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RESOURCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CONTENT_TYPE` int(11) NULL DEFAULT NULL,
  `CONTENT_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CONTENT_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATE_DATE` datetime NULL DEFAULT NULL,
  `IS_READ` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_subscription_notice
-- ----------------------------

-- ----------------------------
-- Table structure for kms_team
-- ----------------------------
DROP TABLE IF EXISTS `kms_team`;
CREATE TABLE `kms_team`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SERIAL_NUMBER` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATOR_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATOR` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATE_DATE` datetime NULL DEFAULT NULL,
  `DEPT_PERMISSION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `COMP_PERMISSION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DISK_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_team
-- ----------------------------

-- ----------------------------
-- Table structure for kms_user_role_set
-- ----------------------------
DROP TABLE IF EXISTS `kms_user_role_set`;
CREATE TABLE `kms_user_role_set`  (
  `ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `USER_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ROLE_ID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kms_user_role_set
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
